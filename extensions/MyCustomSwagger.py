from collections import OrderedDict
from drf_yasg.inspectors.view import SwaggerAutoSchema
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.utils import filter_none, force_real_str, force_serializer_instance
from drf_yasg.openapi import Operation
from drf_yasg import openapi
from django.urls import get_resolver, URLPattern, URLResolver


def get_urlresoler():
    for r in get_resolver().url_patterns:
        if isinstance(r, URLResolver):
            yield str(r.pattern).replace('^', '').replace('$', '').replace('/', ''), r.namespace


class BaseOpenAPISchemaGenerator(OpenAPISchemaGenerator):
    '''重写 OpenAPISchemaGenerator 手动为每个路由添加 tag'''

    def get_schema(self, request=None, public=False):
        '''重写父类方法'''
        swagger = super().get_schema(request, public)
        swagger.tags = [
            {
                'name': item[0],
                'description': item[1]
            } for item in get_urlresoler()
        ]
        # 在这里初始化缓存版本号
        # paths = list(swagger.paths.keys())
        # CacheVersionControl(paths=paths).init_data()
        return swagger


class MySwaggerAutoSchema(SwaggerAutoSchema):

    def get_operation(self, operation_keys=None):
        '''重写父类的方法，进行自定义'''
        
        operation_keys = operation_keys or self.operation_keys

        consumes = self.get_consumes()
        produces = self.get_produces()

        body = self.get_request_body_parameters(consumes)
        query = self.get_query_parameters()
        parameters = body + query
        parameters = filter_none(parameters)
        parameters = self.add_manual_parameters(parameters)

        operation_id = self.get_operation_id(operation_keys)
        summary, description = self.get_summary_and_description()
        if not summary: summary = description # set description and summary
        security = self.get_security()
        assert security is None or isinstance(security, list), "security must be a list of security requirement objects"
        deprecated = self.is_deprecated()
        tags = self.get_tags(operation_keys)

        responses = self.get_responses()

        return Operation(
            operation_id=operation_id,
            description=force_real_str(description),
            summary=force_real_str(summary),
            responses=responses,
            parameters=parameters,
            consumes=consumes,
            produces=produces,
            tags=tags,
            security=security,
            deprecated=deprecated
        )
        
    def get_response_schemas(self, response_serializers):
        """Return the :class:`.openapi.Response` objects calculated for this view.

        :param dict response_serializers: response serializers as returned by :meth:`.get_response_serializers`
        :return: a dictionary of status code to :class:`.Response` object
        :rtype: dict[str, openapi.Response]
        """
        responses = OrderedDict()
        for sc, serializer in response_serializers.items():
            if isinstance(serializer, str):
                response = openapi.Response(
                    description=force_real_str(serializer)
                )
            elif not serializer:
                continue
            elif isinstance(serializer, openapi.Response):
                response = serializer
                if hasattr(response, 'schema') and not isinstance(response.schema, openapi.Schema.OR_REF):
                    serializer = force_serializer_instance(response.schema)
                    response.schema = self.serializer_to_schema(serializer)
            elif isinstance(serializer, openapi.Schema.OR_REF):
                if hasattr(serializer, 'properties'):
                    serializer = serializer.properties.get('data')
                response = openapi.Response(
                    description='',
                    schema= openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties=OrderedDict((
                            ('msg', openapi.Schema(type=openapi.TYPE_STRING)),
                            ('code', openapi.Schema(type=openapi.TYPE_INTEGER)),
                            ('data', serializer),
                        )),
                        required=['data', 'msg', 'code']
                    )
                )
            elif isinstance(serializer, openapi._Ref):
                response = serializer
            else:
                serializer = force_serializer_instance(serializer)
                response = openapi.Response(
                    description='',
                    schema=self.serializer_to_schema(serializer),
                )
            responses[str(sc)] = response
        return responses
    
    # def get_tags(self, operation_keys=None): # 暂时无用
    #     tags = super().get_tags(operation_keys)
    #     print('-' * 128)
    #     print(tags)
    #     print(operation_keys)
    #     if "v1" in tags and operation_keys:
    #         #  `operation_keys` 内容像这样 ['v1', 'prize_join_log', 'create']
    #         tags[0] = operation_keys[1]

    #     return tags


